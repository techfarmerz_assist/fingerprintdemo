package com.vannteqfarm.fingerprint.fingerprintdemo.fingerlib;

import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;

/**
 * Created by MANISH-PC on 9/03/2018.
 */

public interface FingerPrintAuthCallback {

    /**
     * This method will notify the user whenever there is no finger print hardware found on the device.
     * Developer should use any other way of authenticating the user, like pin or password to authenticate the user.
     */
    void onNoFingerPrintHardwareFound();


    void onNoFingerPrintRegistered();

    /**
     * This method will be called if the device is running on android below API 23. As starting from the
     * API 23, android officially got the finger print hardware support, for below marshmallow devices
     * developer should authenticate user by other ways like pin, password etc.
     */
    void onBelowMarshmallow();

    /**
     * This method will occur whenever  user authentication is successful.
     *
     * @param cryptoObject {@link FingerprintManager.CryptoObject} associated with the scanned finger print.
     */
    void onAuthSuccess(FingerprintManager.CryptoObject cryptoObject);

    /**
     * This method will execute whenever any error occurs during the authentication.
     *
     * @param errorCode    Error code for the error occurred. These error code will be from {@link AuthErrorCodes}.
     * @param errorMessage A human-readable error string that can be shown in UI
     * @see AuthErrorCodes
     */
    void onAuthFailed(int errorCode, String errorMessage);
}
