package com.vannteqfarm.fingerprint.fingerprintdemo.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.vannteqfarm.fingerprint.fingerprintdemo.R;

public class AuthSuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_success);
    }
}
